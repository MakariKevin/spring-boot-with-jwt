A simple application using Spring Boot and Spring Security with JWT including Access and Refresh Tokens

RESTful APIs with Spring Boot as backend (server)

## Tools And Technologies Used
- Java 17
- Spring Boot (MVC, JPA, Hibernate)
- Spring Security
- OAuth2 and JWT for authentication
- MySQL Database
- IntelliJ IDEA

## JWT Implementation

![Screenshot 2022-11-17 at 15.49.49.png](./Screenshot 2022-11-17 at 15.49.49.png)

## Output

![Screenshot 2022-11-17 at 23.10.38.png](./Screenshot 2022-11-17 at 23.10.38.png)

![Screenshot 2022-11-17 at 23.13.12.png](./Screenshot 2022-11-17 at 23.13.12.png)

![Screenshot 2022-11-17 at 23.41.13.png](./Screenshot 2022-11-17 at 23.41.13.png)

![Screenshot 2022-11-17 at 23.42.33.png](./Screenshot 2022-11-17 at 23.42.33.png)
