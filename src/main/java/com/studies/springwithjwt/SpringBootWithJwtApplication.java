package com.studies.springwithjwt;

import com.studies.springwithjwt.domain.Role;
import com.studies.springwithjwt.domain.User;
import com.studies.springwithjwt.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class SpringBootWithJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithJwtApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {
			userService.saveRole(new Role(null, "ROLE_USER"));
			userService.saveRole(new Role(null, "ROLE_MANAGER"));
			userService.saveRole(new Role(null, "ROLE_ADMIN"));
			userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new User(null, "James Bond", "james", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Richy Muo", "richy", "1233", new ArrayList<>()));
			userService.saveUser(new User(null, "Grace Dury", "grace", "1232", new ArrayList<>()));
			userService.saveUser(new User(null, "Willy Sage", "willy", "1231", new ArrayList<>()));

			userService.addRoleToUser("james", "ROLE_USER");
			userService.addRoleToUser("james", "ROLE_MANAGER");
			userService.addRoleToUser("richy", "ROLE_MANAGER");
			userService.addRoleToUser("grace", "ROLE_ADMIN");
			userService.addRoleToUser("willy", "ROLE_SUPER_ADMIN");
			userService.addRoleToUser("willy", "ROLE_ADMIN");
			userService.addRoleToUser("willy", "ROLE_USER");
		};
	}
}
